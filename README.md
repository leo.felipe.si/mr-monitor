# MR monitor

### Resumo:
- O programa lê uma lista de projetos no gitlab e verifica os MRs abertos, então comunica via slack webhook
- É possível ter vários escopos (listas de projetos para webhooks diferentes)
- O código carrega arquivos .py dentro do settings como os escopos
- Um escopo é informado da seguinte maneira: `SIMPLE_SETTINGS=settings.meu_escopo`
- A execução do programa se dá da seguinte maneira: `SIMPLE_SETTINGS=settings.meu_escopo python3 monitor.py`
- O escopo `base.py` pode ser herdado para reaproveitamento genérico em outros escopos
- É preciso ter um [token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) para a API do gitlab: `settings.base - REPOSITORY_TOKEN`
- É preciso ter um [webhook](https://api.slack.com/apps/) para a API do slack: `settings.base - SLACK_WEBHOOK`

### DA INSTALAÇÃO
```
python=python3
```
```
make install
```

### DO PROJETO
```
monitor.py  # script principal
utils  # métodos auxilares
settings # diretório de escopos
```

### DAS CONFIGURAÇÕES

Dentro de settings ficam os arquivos de escopo.
Você pode configurar a lista de projetos e integração com o [gitlab](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html), interação com [slack](https://api.slack.com/apps/) e as mensagens de comunicação.
```
PROJECTS = [
    {"id": <id_gitlab_projeto1>, "name": <nome para o projeto1>},
    {"id": <id_gitlab_projeto2>, "name": <nome para o projeto2>},
    {"id": <id_gitlab_projeto3>, "name": <nome para o projeto3>}
]
```
Os projetos também podem ser informados via variável de ambiente da seguinte forma:
```
export PROJECTS="id_gitlab_projeto1:nome_projeto1;id_gitlab_projeto2:nome_projeto2;id_gitlab_projeto3:nome_projeto3"

```

_As variáveis de [TOKEN](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) e [WEBHOOK](https://api.slack.com/apps/) recomenda-se configurar como var envs._

### DA EXECUÇÃO:

É preciso informar qual o escopo para execução.
Pode ser configurado como variável de ambiente: `export SIMPLE_SETTINGS=settings.base`. Sendo assim as configurações seguidas estarão em settings/base.py
Caso queria informar o escopo em tempo de execulção:
```
SIMPLE_SETTINGS=settings.meu_escopo python3 monitor.py
```
