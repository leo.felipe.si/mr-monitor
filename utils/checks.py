import datetime
import time

import requests
from simple_settings import settings


def is_today_weekday():
    day = datetime.datetime.today().weekday()
    return 0 <= day <= 4


def get_hour(system_time=True):
    if system_time:
        return datetime.datetime.now().hour
    retry = 3
    while retry:
        try:
            resp = requests.get(settings.URL_TIME).json()
            hour = int(
                resp['datetime'].split('T')[1].split('.')[0].split(':')[0]
            )
            return hour
        except Exception:
            retry -= 1
            time.sleep(2)
    return 10


def get_time_shift(system_time=True):
    hour = get_hour(system_time)
    if 5 <= hour < 12:
        return "morning"
    elif 12 <= hour < 18:
        return "evening"
    return "night"
