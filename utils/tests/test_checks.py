import time

import mock
import pytest
from simple_settings import settings

from utils.checks import get_hour, get_time_shift, is_today_weekday


class TestChecks:

    @pytest.fixture
    def sp_time(self):
        return {'datetime': '2020-11-10T08:00:0.0'}

    def test_get_hour_from_internet(self, sp_time, requests_mock):
        requests_mock.get(settings.URL_TIME, json=sp_time)
        assert get_hour(system_time=False) == 8

    @mock.patch('time.sleep')
    def test_get_hour_when_internet_error(self, sp_time, requests_mock):
        time.sleep.return_value = 0
        requests_mock.get(settings.URL_TIME, json=None)
        assert get_hour(system_time=False) == 10

    def test_get_hour_from_system(self):
        assert type(get_hour(system_time=True)) == int
