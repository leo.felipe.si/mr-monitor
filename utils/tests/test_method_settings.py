from utils.method_settings import set_varenvs


class TestMethodSettings:

    def test_set_varenvs(self, monkeypatch):
        monkeypatch.setenv("PROJECTS_LIST", "1:projeto 1;2:projeto 2")
        assert (
            set_varenvs([]) == (
                [{
                    "id": 1,
                    "name": "projeto 1"
                }, {
                    "id": 2,
                    "name": "projeto 2"
                }]
            )
        )
