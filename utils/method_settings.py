import os


def set_varenvs(set_vars):
    projects = []
    PROJECTS_VAR_ENV = os.getenv("PROJECTS_LIST")

    if PROJECTS_VAR_ENV:
        project_tuple_settings = PROJECTS_VAR_ENV.split(';')
        for project_tuple in project_tuple_settings:
            project_id, project_name = project_tuple.split(':')
            projects.append({
                "name": project_name.strip(),
                "id": int(project_id.strip())
            })
    elif set_vars:
        projects = set_vars

    return projects
