import random
import sys

import requests
from simple_settings import settings

from utils.checks import get_time_shift, is_today_weekday


def load_projects_info():
    urls = []
    for p in settings.PROJECTS:
        pid = p['id']
        url = settings.REPOSITORY_URL_TEMPLATE.format(
            settings.REPOSITORY_HOST,
            settings.REPOSITORY_ENDPOINT.format(id=pid),
            settings.REPOSITORY_MR_ENDPOINT,
            settings.REPOSITORY_TOKEN
        )
        urls.append({
            "id": p["id"],
            "name": p["name"],
            "url": url
        })
    return urls


def get_mrs_from_project(project):
    url = project["url"]
    name = project["name"]

    resp = requests.get(url)
    if resp.status_code != 200:
        print("Error url request:", url)
        sys.exit(1)
    resp = resp.json()

    mrs_project = {'name': name, 'mrs': []}
    for mr in resp:
        if settings.APPROVAL_CRITERIA(mr):
            mrs_project['mrs'].append(
                {
                    'name': mr['title'],
                    'description': mr['description'],
                    'url': mr['web_url']
                }
            )
    return mrs_project


def bind_mrs_message(mrs):
    msg = ""
    for project in mrs:
        msg += "\n> *{}:*\n".format(project['name'])
        for mr in project['mrs']:
            description = ""
            if mr['description']:
                description = (
                    ' - _' +
                    ' '.join(mr['description'].split()[0:7]) +
                    (' ...' if (
                        len(mr['description'].split()) > 7
                    ) else '') + '_'
                )
            msg += ">     <{}|• {}>{}\n".format(
                mr['url'], mr['name'], description
            )
    return msg


def bind_greeting_message():
    shift = get_time_shift(settings.USE_SYSTEM_TIME)

    msg_idx = random.randint(0, len(settings.GREETINGS[shift]["greetings"])-1)
    greeting = settings.GREETINGS[shift]["greetings"][msg_idx]

    msg = settings.GREETINGS[shift]["template"].format(greeting)
    msg += "{}\n\n" + settings.NOTIFICATION_CONTACTS
    return msg


if __name__ == "__main__":
    if not is_today_weekday():
        sys.exit(0)
    mrs = []

    for project in load_projects_info():
        mrs_project = get_mrs_from_project(project)
        if len(mrs_project['mrs']) > 0:
            mrs.append(mrs_project)

    if len(mrs) == 0:
        sys.exit(0)

    message_body = bind_greeting_message()
    mr_message = bind_mrs_message(mrs)

    msg = message_body.format(mr_message)

    payload = {
       "text": settings.NOTIFICATION_TEXT,
       "blocks": [{
           "type": "section",
           "text": {
               "type": "mrkdwn",
               "text": msg
           }
       }]
    }
    requests.post(
        url=settings.SLACK_WEBHOOK,
        json=payload
    )
