export SIMPLE_SETTINGS=settings.base

.DEFAULT_GOAL := help

help:
	@echo "$$(tput bold)Commands:$$(tput sgr0)";echo;
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "%-30s%s\n", $$1, $$2}'

install: ## Install all dependencies
	@pip3 install -r requirements.txt

run-monitor: ## Run analyzer with dev configs [module]
	@if [ "$(scope)" != "" ];\
		then SIMPLE_SETTINGS=settings.$(scope) python3 monitor.py;\
	else \
		SIMPLE_SETTINGS=settings.base python3 monitor.py;\
	fi

lint: ## Run lint
	@flake8 .
	@isort --check

test: ## Run tests
	@SIMPLE_SETTINGS=settings.test py.test -xs -vv . -m 'not integration'

test-matching: ## Run tests
	@SIMPLE_SETTINGS=settings.test py.test -rxs --pdb -k$(Q) . -m 'not integration'

coverage: ## Run tests and collect coverage report
	@SIMPLE_SETTINGS=settings.test coverage run -m py.test && coverage report
