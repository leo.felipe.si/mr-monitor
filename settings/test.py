import os

from utils.method_settings import set_varenvs

from .base import *  # noqa

PROJECTS = [
    {"id": 1, "name": "Projeto #1"}
]

PROJECTS = set_varenvs(PROJECTS)

REPOSITORY_TOKEN = "FAKE_TOKEN"
REPOSITORY_ENDPOINT = "{id}"
REPOSITORY_MR_ENDPOINT = "mrs_opened"
REPOSITORY_URL_TEMPLATE = "{}/{}/{}&private_token={}"

SLACK_WEBHOOK = os.getenv("SLACK_WEBHOOK")

APPROVAL_UPVOTE = os.getenv("APPROVAL_UPVOTE", 3)

USE_SYSTEM_TIME = False


def check_mr(mr):
    title = (mr.get("title") or "").lower()
    upvotes = int(mr.get("upvotes")) or 2
    labels = mr.get("labels") or []

    return (
        'wip' not in title
        and upvotes < APPROVAL_UPVOTE
        and 'blocked' not in labels
        and 'approved' not in labels
    )


APPROVAL_CRITERIA = check_mr

NOTIFICATION_TEXT = "@time: MRs update"
NOTIFICATION_CONTACTS = "Valeu @time!!"

MORNING_MSG_TEMPLATE = "Ola, {}:"
MORNING_GREETINGS = [
    "como vao?"
]
GREETINGS = {
    "morning": {
        "greetings": MORNING_GREETINGS,
        "template": MORNING_MSG_TEMPLATE
    }
}
