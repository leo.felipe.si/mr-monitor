import os

from utils.method_settings import set_varenvs

PROJECTS = [
    # {"id": <int>, "name": <a custom text>}
]

PROJECTS = set_varenvs(PROJECTS)

REPOSITORY_HOST = "https://gitlab.com"
REPOSITORY_TOKEN = os.getenv("REPOSITORY_TOKEN")
REPOSITORY_ENDPOINT = "api/v4/projects/{id}"
REPOSITORY_MR_ENDPOINT = "merge_requests?state=opened"
REPOSITORY_URL_TEMPLATE = "{}/{}/{}&private_token={}"

SLACK_WEBHOOK = os.getenv("SLACK_WEBHOOK")

APPROVAL_UPVOTE = os.getenv("APPROVAL_UPVOTE", 3)

USE_SYSTEM_TIME = os.getenv("USE_SYSTEM_TIME", True)


def check_mr(mr):
    title = (mr.get("title") or "").lower()
    upvotes = int(mr.get("upvotes")) or 2
    labels = mr.get("labels") or []

    return (
        'wip' not in title
        and upvotes < APPROVAL_UPVOTE
        and 'blocked' not in labels
        and 'approved' not in labels
    )


APPROVAL_CRITERIA = check_mr

NOTIFICATION_TEXT = "@time: MRs update"
NOTIFICATION_CONTACTS = "Valeu @time!!"

MORNING_MSG_TEMPLATE = "Oi pessoal, bom dia!\n\n{}\nVê só o que tá rolando:\n"
MORNING_GREETINGS = [
    "Que tal já começar dando uma olhadinha nos MRs abertos? =)",
    "Antes de começarem, alguém conseguiria dar uma olhada nos MRs abertos? xD",  # noqa
    "Tem uns MRs abertos. Alguém consegue dar uma validada? ^^ ",
    "Conseguiriam dar uma olhada nos MRs que estão abertos?"
]

EVENING_MSG_TEMPLATE = "Oi pessoal, boa tarde!\n\n{}\nVê só o que tá rolando:\n"  # noqa
EVENING_GREETINGS = [
    "Conseguiriam tirar um minutinho para ver os MRs que estão abertos? ;)",
    "Só pra lembrar que tem uns MRs pra validar. xD",
    "Tem uns MRs pra validação. Algúem consegue dar uma olhada? =)",
    "Antes de retomarem, algúem consegue ver os MRs abertos? =)"
]

NIGHT_MSG_TEMPLATE = "Oi pessoal, boa noite!\n\n{}\nVê só o que tá rolando:\n"
NIGHT_GREETINGS = [
    "Será se rola um momentinho para ver ums MRs que ainda estão abertos? ^^"
]

GREETINGS = {
    "morning": {
        "greetings": MORNING_GREETINGS,
        "template": MORNING_MSG_TEMPLATE
    },
    "evening": {
        "greetings": EVENING_GREETINGS,
        "template": EVENING_MSG_TEMPLATE
    },
    "night": {
        "greetings": NIGHT_GREETINGS,
        "template": NIGHT_MSG_TEMPLATE
    }
}

URL_TIME = "http://worldtimeapi.org/api/timezone/America/Sao_Paulo"
