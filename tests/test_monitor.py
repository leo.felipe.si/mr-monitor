import pytest
from simple_settings import settings

from monitor import (bind_greeting_message, bind_mrs_message,
                     get_mrs_from_project, load_projects_info)


class TestMRMonitor:

    @pytest.fixture
    def expected_project_info(self):
        return {
            'id': 1,
            'name': 'Projeto #1',
            'url': 'https://gitlab.com/1/mrs_opened&private_token=FAKE_TOKEN'
        }

    @pytest.fixture
    def mr_response(self):
        return [{
            'id': 1,
            'iid': 1,
            'project_id': 1,
            'title': 'MR #1',
            'description': 'Exemplo de description #1',
            'state': 'opened',
            'upvotes': 0,
            'labels': [],
            'web_url': 'www.gitlab.com/mr/1'
        }]

    @pytest.fixture
    def sp_time(self):
        return {'datetime': '2020-11-10T10:10:10.0'}

    def test_load_projects_info(self, expected_project_info):
        assert load_projects_info() == [expected_project_info]

    def test_get_mrs_from_project(
        self, expected_project_info, requests_mock, mr_response
    ):

        requests_mock.get(expected_project_info["url"], json=mr_response)

        assert get_mrs_from_project(expected_project_info) == {
            'name': 'Projeto #1',
            'mrs': [{
                'name': 'MR #1',
                'description': 'Exemplo de description #1',
                'url': 'www.gitlab.com/mr/1'
            }]
        }

    def test_bind_mrs_message(self):
        expected_msg = (
            "\n> *Projeto #1:*\n>     <www.gitlab.com/mr/1|"
            "• MR #1> - _Exemplo de description #1_\n"
        )
        binded_message = bind_mrs_message([{
            'name': 'Projeto #1',
            'mrs': [{
                'name': 'MR #1',
                'description': 'Exemplo de description #1',
                'url': 'www.gitlab.com/mr/1'
                }]
            }]
        )
        assert expected_msg == binded_message

    def test_bind_greeting_message(self, requests_mock, sp_time):
        requests_mock.get(settings.URL_TIME, json=sp_time)
        expected_message = "Ola, como vao?:{}\n\nValeu @time!!"
        assert bind_greeting_message() == expected_message
